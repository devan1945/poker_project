import cards_slice
import pyautogui
import time
import random
import math
pyautogui.PAUSE = 1.5
l=["high card", "one pair", "two pair", "set", "straight", "flush", "full house", "care", "straight flush", "royal flush"]
l.reverse()
class P_Player:
    def __init__(self, hand_cards=[],decision="C/C",variants={}):
        self.hand_cards=hand_cards
        self.decision=decision
        self.variants={"high card":0, "one pair":0, "two pair":0, "set":0, "straight":0, "flush":0, "full house":0, "care":0, "straight flush":0, "royal flush":0}
    
    def run_game(self):
        while True:
            try:                   
                self.hand_cards=cards_slice.handcheck()
                if self.hand_cards == []:
                    raise Exception("Карты не сданы")
                if (cards_slice.if_check_call())and(bool(cards_slice.tblcard3check())==False):
                    self.choose_decision_hand()

                print(Player1.hand_cards)
                print(self.decision)
                print("run_game")
                if (bool(cards_slice.tblcard3check()))and(self.decision !="F"):
                    self.run_flop()
                else:
                    time.sleep(2)
            except Exception as e:
                print(e)
                time.sleep(5)  

    def run_flop(self):
        while True:
            if (self.newBoard.flop != [])and(self.decision =="F"):
                break
            try:
                if (bool(cards_slice.tblcard3check())==False):
                    raise Exception("Флоп не сдан")
                if (cards_slice.if_check_call()):
                    self.newBoard.flop=cards_slice.tblcard3check()
                    print(self.newBoard.flop)
                    print("run_flop")
                    self.choose_decision_board(cards=self.newBoard.flop)                        
                if (bool(cards_slice.tblcard4check()))and(self.decision !="F"):
                    self.run_tern()
                    break
                else:
                    time.sleep(2)
            except Exception as e:
                print(e)
                break
            

    def run_tern(self):
        while True:
            if (self.newBoard.tern != [])and(self.decision =="F"):
                break
            try:
                
                if (bool(cards_slice.tblcard4check())==False):
                    raise Exception("Терн не сдан")
                if (cards_slice.if_check_call()):
                    self.newBoard.tern=cards_slice.tblcard4check()
                    print(self.newBoard.tern)
                    print("run_tern")
                    self.choose_decision_board(cards=self.newBoard.tern)

                if (bool(cards_slice.tblcard5check()))and(self.decision !="F"):
                    self.run_river()
                else:
                    time.sleep(2)
            except Exception as e:
                print(e)
                break
                time.sleep(5)
                  

    def run_river(self):
        while True:
            if (self.newBoard.river != [])and(self.decision =="F"):
                break
            try:
                
                if (bool(cards_slice.tblcard5check())==False):
                    raise Exception("Ривер не сдан")
                    print(self.newBoard.river)
                    print("run_river")
                else:
                    time.sleep(2)  
                if (cards_slice.if_check_call()):
                    self.newBoard.river=cards_slice.tblcard5check()
                    self.choose_decision_board(cards=self.newBoard.river)                      
            except Exception as e:
                print(e)
                break
                time.sleep(5)

    def choose_decision_hand(self): #fold, call==check, raise
        self.newBoard=Board()
        self.decision = "C/C" 
        card1 = [int(self.hand_cards[0][0]),(self.hand_cards[0][1])]
        card2 = [int(self.hand_cards[1][0]),(self.hand_cards[1][1])]
        if ((card1[0]<10) and (card2[0]<10) and ((card1[1]!=card2[1])or((card1[1]==card2[1])and(math.fabs(card1[0]-card2[0])>2)))):
            self.decision = "F"
        if (cards_slice.if_fold()==False)and(self.decision == "F"):
            self.decision = "C/C"    
        
        if (((card1[0]==card2[0])and(card1[0]>5))or((card1[0]>9 or card2[0]>9)and(card1[1]==card2[1]))or(card1[0]>9 and card2[0]>9)):
            self.decision = "B/R"
        self.make_decision()   
        if self.newBoard.combination[0][0]<2:
            self.newBoard.combination[0][0]+=2  
            self.newBoard.combination[card1[0]][0]=card1[0]
            self.newBoard.combination[card1[0]][1]+=1
            self.newBoard.combination[card1[0]][2]|=(self.mast_to_number(card1[1])*16)
            self.newBoard.combination[card1[0]][3]+=1
            self.newBoard.combination[card2[0]][0]=card2[0]
            self.newBoard.combination[card2[0]][1]+=1
            self.newBoard.combination[card2[0]][2]|=(self.mast_to_number(card2[1])*16)
            self.newBoard.combination[card2[0]][3]+=1

        
        

    def choose_decision_board(self,cards=[]):
        for i in cards:
            j=int(i[0])
            if (self.mast_to_number(i[1]) | self.newBoard.combination[j][2])!=self.newBoard.combination[j][2]:
                self.newBoard.combination[0][0]+=1   # считаем количесвто сданных карт
                self.newBoard.combination[j][0]=j
                self.newBoard.combination[j][1]+=1
                self.newBoard.combination[j][2]|=self.mast_to_number(i[1])
        print(self.newBoard.combination)
        self.ready_hand()
        self.make_decision()



    def make_decision(self):
        y=random.randrange(668,709)
        x=0
        if (self.decision=="C/C"):
            x=random.randrange(686,822)
        if (self.decision=="F"):
            x=random.randrange(545,647)
        if (self.decision=="B/R"):
            x=random.randrange(859,999)    
        cp=pyautogui.position()
        t=math.ceil((math.sqrt((cp[0]-x)**2+(cp[1]-y)**2))/1500)
        pyautogui.moveTo(x, y, t, pyautogui.easeOutQuad)
        pyautogui.click()

           
        #pyautogui.moveTo()

    def mast_to_number(m,c):
        if c == "C": 
            return (2)
        if c == "D": 
            return (4)
        if c == "H": 
            return (8)
        if c == "S": 
            return (16)

    #Straight
    def straight(self):
        num=0
        i=1
        max=0
        flag=0
        flmax=0
        while(i<15):
            if((self.newBoard.combination[i][1]>0)and(i>1)):
                num+=1
                if (self.newBoard.combination[i][3]==1):
                    flag=i
            else:
                if (max<num):
                    max=num
                    flmax=flag
                num=0
                flag=0
            if(i==1):
                if (self.newBoard.combination[14][1]>0):
                    num+=1
                    if (self.newBoard.combination[i][3]==1):
                        flag=i    
                else:
                    if (max<num):
                        max=num
                        flmax=flag
                    num=0
                    flag=0
            i+=1
            
        if(max<num):
            max=num
            flmax=flag
        num=0
        i=0
        flag=0
        if(flmax>=max):
            while(i<max):
                if((self.newBoard.combination[flmax-i][3]>0)and(flmax-i!=1)):
                    flag+=1
                if((self.newBoard.combination[14][3]>0)and((flmax-i)==1)):
                    flag+=1
                i+=1

        straight=0 #0-ничего, 1-стрит, 2-открытый стрит(2-х сторонний) 3-гатшот  4-натс стрит на столе 5-стрит на столе
        if((max>=5)and(flmax==14)and(flag==0)):
            straight=4
        if((max>=5)and(flmax<14)and(flag==0)):
            straight=5
        if((max>=5)and(flag>0)):
            straight=1
        if((max==4)and(flag>0)and(flmax<14)):
            straight=2
        if((max==4)and(flag>0)and(flmax==14)):
            straight=3
        if((max==4)and(flag>0)and(flmax==4)):
            straight=3
        
        return straight

        """Flush"""
    def flush(self):
        flush=0 #0-ничего 1-флэшь 2-флэшь дро  3-флэшь на столе(nothing cards)
        flush1=0
        j=2
        while(j<=16):
            flush=0
            flag=0
            i=2
            num=0
            flmax=0
            flag1=0
            while(i<15):
                if ((self.newBoard.combination[i][2]& j)==j):    
                    flmax=i
                    num+=1
                if ((self.newBoard.combination[i][2]&(j*16))==j*16):    
                    flag1+=1
                    flag=i
                i+=1
            if((num==5)and(flag==0)):
                flush=3
            if((num>=5)and(flmax==14)and(flag>=13)): 
                flush=1
            if((num>=5)and(flmax==14)and((self.newBoard.combination[13][2]& j)==j)and(flag==12)):
                flush=1
            if((num>=5)and(flmax==14)and((self.newBoard.combination[13][2]& j)==j)and((self.newBoard.combination[12][2]& j)==j)and(flag==11)):
                flush=1
            if((num>=5)and(flmax==14)and((self.newBoard.combination[13][2]& j)==j)and((self.newBoard.combination[12][2]& j)==j)and((self.newBoard.combination[11][2]& j)==j)and(flag==10)):
                flush=1
            if((num>=5)and(flmax==14)and((self.newBoard.combination[13][2]& j)==j)and((self.newBoard.combination[12][2]& j)==j)and((self.newBoard.combination[11][2]& j)==j)and((self.newBoard.combination[10][2]& j)==j)):
                flush=1
            if((num==5)and(flag1==2)): 
                flush=1
            if((num==4)and(flag>0)):
                flush=2
            if((flush>flush1)and(flush!=0)):
                flush1=flush
            print(flush)

            j*=2
        return flush1

    """Others combinations"""
    def others(self):
        pair=0
        quad=0
        top=0
        over=0
        set1=0
        set=0
        quad1=0
        pair1=0
        i=2
        print(self.newBoard.combination)
        while(i<15):
            if ((self.newBoard.combination[i][1]>=1)and(self.newBoard.combination[i][3]==0)):
                top=0
                over=0
            if ((self.newBoard.combination[i][1]==2)and(self.newBoard.combination[i][3]==0)):
                pair+=1
            if ((self.newBoard.combination[i][1]==2)and(self.newBoard.combination[i][3]==1)):
                pair1+=1
                pair+=1
                top=1
            if ((self.newBoard.combination[i][1]==2)and(self.newBoard.combination[i][3]==2)):
                pair1+=1
                pair+=1
                over=1
            if (self.newBoard.combination[i][1]==3):
                set+=1
            if (self.newBoard.combination[i][1]==4):
                quad+=1
            if ((self.newBoard.combination[i][1]==3)and(self.newBoard.combination[i][3]>=1)): 
                set1+=1
            if ((self.newBoard.combination[i][1]==4)and(self.newBoard.combination[i][3]>0)): 
                quad1+=1
            i+=1
            
        kicker=0
        if (top>0):
            for i in range (2,15):
                if ((self.newBoard.combination[i][1]==1)and(self.newBoard.combination[i][3]==1)): 
                    kicker=i
        others = {"pair1":pair1, "pair":pair, "top":top, "over":over, "kicker":kicker, "set1":set1, "set":set, "quad1":quad1, "quad":quad}
        return others           
                

    def ready_hand(self):
        flush=self.flush()
        straight=self.straight()
        others=self.others()  
        print(flush)
        print(straight)
        print(others)
        readyhand=0 #0-не готовая , 1-готовая комбинация, 2-добираемая
        if ((others["pair"]==1)and((others["top"]==1)or(others["over"]==1))):
            readyhand=1
            print("pairtopover")
        if ((others["pair"]==1)and(others["top"]==1)and(others["over"]==0)): 
            readyhand=1
            print("pairtopover")
        if ((others["pair"]>=2)and((others["top"]==1)or(others["over"]==1))):
            readyhand=1
            print("pairtopover")
        if ((others["pair"]==2)and(others["top"]==1)and(others["kicker"]<11)and(others["over"]==0)):
            readyhand=0
            print("pairtopkickerover")
        if ((others["pair"]==2)and((others["top"]==0)or(others["over"]==0))and(others["pair1"]==2)):
            readyhand=1
            print("pairtopoverpair1")
        if ((others["pair"]>2)and((others["top"]==0)or(others["over"]==0))):
            readyhand=0
            print("pairtopover")
        if ((others["set"]==1)and(others["top"]==0)and(others["over"]==0)and(others["pair1"]==0)):
            readyhand=0
            print("settopoverpair1")
        if (others["set"]==1):
            readyhand=1
            print("set")
        if (others["set1"]==1):
            readyhand=1
            print("set1")
        if (straight==5):
            readyhand=0
        if (straight==4):
            readyhand=1
        if (straight==2):
            readyhand=2
        if (straight==1):
            readyhand=1
        if (flush==3):
            readyhand=0
        if ((flush==2)):
            readyhand=2
        if (flush==1):
            readyhand=1
        if ((others["set"]==1)and((others["top"]==1)or(others["over"]==1))):
            readyhand=1
            print("settopover")
        if ((others["set"]==1)and(others["top"]==0)and(others["over"]==0)and(others["pair1"]>0)):
            readyhand=1
            print("settopoverpair1")
        if ((others["set"]==1)and(others["top"]==0)and(others["over"]==0)and(others["pair1"]==0)and(others["pair"]>0)):
            readyhand=0 
            print("settopoverpair1pair")
        if ((others["set1"]==1)and(others["pair"]>0)):
            readyhand=1
            print("set1pair")
        if (others["quad"]==1): 
            readyhand=1
        if readyhand==1:
            self.decision="B/R"
        if readyhand==2:
            self.decision="C/C"
        if readyhand==0:
            self.decision="F"
        if (cards_slice.if_fold()==False)and(self.decision=="F"):
            self.decision="C/C"

    
                  
        
class Board:
    def __init__(self, flop=[], tern=[], river=[], combination=[]):
        self.flop=flop
        self.tern=tern
        self.river=river 
        self.combination=[[0,0,0,0] for i in range(15)]    

if __name__ == '__main__':
    Player1=P_Player()
    
    Player1.run_game()    