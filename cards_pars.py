# Import necessary packages
import cv2
import numpy as np
import time
import os
import comp_cards
import cards_slice


### ---- INITIALIZATION ---- ###
# Define constants and initialize variables



# Load the train rank and suit images
path = os.path.dirname(os.path.abspath(__file__))
train_ranks = comp_cards.load_ranks( path + '/Card_I/')
train_suits = comp_cards.load_suits( path + '/Card_I/')


### ---- MAIN LOOP ---- ###
# The main loop repeatedly grabs frames from the video stream
# and processes them to find and identify playing cards.


# Begin capturing frames
#while cam_quit == 0:

# Grab frame from video stream

def pars():
    cards_list=list()
    if (cards_slice.handcheck()):
        image1 = cv2.imread("cards_slice/handcard1.jpg",0)
        image2 = cv2.imread("cards_slice/handcard2.jpg",0)
        
            # Pre-process camera image (gray, blur, and threshold it)
            #pre_proc = comp_cards.preprocess_image(image1)
    	

            # For each contour detected:
            # Create a card object from the contour and append it to the list of cards.
            # preprocess_card function takes the card contour and contour and
            # determines the cards properties (corner points, etc). It generates a
            # flattened 200x300 image of the card, and isolates the card's
            # suit and rank from the image.

        card = (comp_cards.preprocess_card(image1))
        card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
        #comp_cards.draw_results(card)
        cards_list.append(comp_cards.draw_results(card))

        card = (comp_cards.preprocess_card(image2))
        card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
        #comp_cards.draw_results(card)
        cards_list.append(comp_cards.draw_results(card))

        if (cards_slice.tblcard3check()):
            image1 = cv2.imread("cards_slice/tblc1.jpg",0)
            image2 = cv2.imread("cards_slice/tblc2.jpg",0)
            image3 = cv2.imread("cards_slice/tblc3.jpg",0)
            # Pre-process camera image (gray, blur, and threshold it)
            #pre_proc = comp_cards.preprocess_image(image1)
            card = (comp_cards.preprocess_card(image1))
            card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
            #comp_cards.draw_results(card)
            cards_list.append(comp_cards.draw_results(card))

            card = (comp_cards.preprocess_card(image2))
            card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
            #comp_cards.draw_results(card)
            cards_list.append(comp_cards.draw_results(card))

            card = (comp_cards.preprocess_card(image3))
            card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
            #comp_cards.draw_results(card)
            cards_list.append(comp_cards.draw_results(card))

            if (cards_slice.tblcard4check()):
                image3 = cv2.imread("cards_slice/tblc4.jpg",0)
                card = (comp_cards.preprocess_card(image3))
                card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
                #comp_cards.draw_results(card)
                cards_list.append(comp_cards.draw_results(card))

                if (cards_slice.tblcard5check()):
                    image3 = cv2.imread("cards_slice/tblc5.jpg",0)
                    card = (comp_cards.preprocess_card(image3))
                    card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
                    #comp_cards.draw_results(card)
                    cards_list.append(comp_cards.draw_results(card))
    return(cards_list)


