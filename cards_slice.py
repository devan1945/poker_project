from PIL import Image
import cv2
import numpy as np
import pyscreenshot as ImageGrab
import comp_cards
import time
import os


path = os.path.dirname(os.path.abspath(__file__))
train_ranks = comp_cards.load_ranks( path + '/Card_I/')
train_suits = comp_cards.load_suits( path + '/Card_I/')


def screenshot():

	snapshot = ImageGrab.grab()
	save_path = ".\MySnapshot.jpg"
	snapshot.save(save_path)


def handcheck():
	screenshot()
	img = Image.open("MySnapshot.jpg")
	im = cv2.imread("MySnapshot.jpg")
	ans = list()
	hc2 = im[506,519]	#right card in hand
	if (hc2[0] >= 220 and hc2[1] >= 220 and hc2[2] >= 220):
		img2 = img.crop((449,463,466,500))
		img2.save("cards_slice/handcard1.jpg")
		image1 = cv2.imread("cards_slice/handcard1.jpg",0)

		img2 = img.crop((512,464,528,498))
		img2.save("cards_slice/handcard2.jpg")
		image2 = cv2.imread("cards_slice/handcard2.jpg",0)

		card = (comp_cards.preprocess_card(image1))
		card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
		ans.append(comp_cards.draw_results(card))

		card = (comp_cards.preprocess_card(image2))
		card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
		ans.append(comp_cards.draw_results(card))
	return ans

def tblcard3check():
	screenshot()
	img = Image.open("MySnapshot.jpg")
	im = cv2.imread("MySnapshot.jpg")
	ans = list()
	tblc3=im[313,365]
	if (tblc3[0] >= 220 and tblc3[1] >= 220 and tblc3[2] >= 220):
		img2 = img.crop((354,260,371,298))
		img2.save("cards_slice/tblc1.jpg")
		image1 = cv2.imread("cards_slice/tblc1.jpg",0)
		card = (comp_cards.preprocess_card(image1))
		card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
		ans.append(comp_cards.draw_results(card))

		img2 = img.crop((419,260,436,298))
		img2.save("cards_slice/tblc2.jpg")
		image2 = cv2.imread("cards_slice/tblc2.jpg",0)
		card = (comp_cards.preprocess_card(image2))
		card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
		ans.append(comp_cards.draw_results(card))

		img2 = img.crop((484,260,501,298))
		img2.save("cards_slice/tblc3.jpg")
		image3 = cv2.imread("cards_slice/tblc3.jpg",0)
		card = (comp_cards.preprocess_card(image3))
		card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)
		ans.append(comp_cards.draw_results(card)) 
	return ans

def tblcard4check():
	screenshot()
	img = Image.open("MySnapshot.jpg")
	im = cv2.imread("MySnapshot.jpg")
	ans = list()
	tblc4=im[319,557]
	if (tblc4[0] >= 220 and tblc4[1] >= 220 and tblc4[2] >= 220):
		img2 = img.crop((549,260,566,298))
		img2.save("cards_slice/tblc4.jpg")
		image3 = cv2.imread("cards_slice/tblc4.jpg",0)
		card = (comp_cards.preprocess_card(image3))                
		card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)     
		ans.append(comp_cards.draw_results(card))                
	return ans



def tblcard5check():
	screenshot()
	img = Image.open("MySnapshot.jpg")
	im = cv2.imread("MySnapshot.jpg")
	ans = list()
	tblc5=im[310,628]
	if (tblc5[0] >= 220 and tblc5[1] >= 220 and tblc5[2] >= 220):
		img2 = img.crop((614,260,631,298))
		img2.save("cards_slice/tblc5.jpg")
		image3 = cv2.imread("cards_slice/tblc5.jpg",0)
		card = (comp_cards.preprocess_card(image3))                    
		card.best_rank_match,card.best_suit_match,card.rank_diff,card.suit_diff = comp_cards.match_card(card,train_ranks,train_suits)            
		ans.append(comp_cards.draw_results(card))                    
	return ans

def if_fold():
	screenshot()
	img = Image.open("MySnapshot.jpg")
	im = cv2.imread("MySnapshot.jpg")
	tblfold=im[690,613]
	if (tblfold[2]>=60):
		return True
	return False

def if_check_call():
	screenshot()
	img = Image.open("MySnapshot.jpg")
	im = cv2.imread("MySnapshot.jpg")
	tblcc=im[673,727]
	if (tblcc[2]>=90):
		print(tblcc[2])
		return True
	else:
		print(tblcc[2])
		return False

"""def if_check_call():
	screenshot()
	img = Image.open("MySnapshot.jpg")
	im = cv2.imread("MySnapshot.jpg")
	tblfold=im[690,613]
	if (tblfold[2]>=60):
		return True
	return False"""